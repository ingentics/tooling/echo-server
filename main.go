package main

import (
	"fmt"
	"gitlab.com/ingentics/tooling/echo-server.git/pkg/server"
	"gopkg.in/yaml.v2"
	"log"
	"os"
)

const filename = "conf.yaml"

func main() {
	app, err := createApp()
	if err != nil {
		log.Fatal(err)
	}

	if err := app.Start(); err != nil {
		log.Fatal(err)
	}
}

func createApp() (*server.EchoServer, error) {
	// We read the configuration file
	raw, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("could not open `%s` file: %v", filename, err)
	}
	app := &server.EchoServer{}
	err = yaml.Unmarshal(raw, app)
	if err != nil {
		return nil, fmt.Errorf("could parse `%s` file: %v", filename, err)
	}

	return app, nil
}
