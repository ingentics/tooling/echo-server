# Usage

## With without waiting groups

```go
port := 12345

server := NewEchoServer(fmt.Sprintf("127.0.0.1:%d", port),
	// Here you add your faked endpoints
    NewEndpoint("GET", "/test/1", `{"test": "1"}`),
    NewEndpoint("POST", "/test/2", `{"test": "2"}`),
)

defer server.Stop()
if err := server.Start(); err != nil {
    t.Fatal(err)
}

// ...
response, err := http.Get(url)
// ...

```

## With waiting groups

```go
var wg sync.WaitGroup
port := 12345

server := NewEchoServer(fmt.Sprintf("127.0.0.1:%d", port),
	// Here you add your faked endpoints
    NewEndpoint("GET", "/test/1", `{"test": "1"}`),
    NewEndpoint("POST", "/test/2", `{"test": "2"}`),
)

defer server.Stop()
if err := server.StartWithWaitingGroup(&wg); err != nil {
    t.Fatal(err)
}

// ...
wg.Add(1)
response, err := http.Get(url)
// ...

```