package server

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
)

type EchoServer struct {
	Address   string      `json:"address"`
	Endpoints []*Endpoint `json:"endpoints"`

	listener net.Listener
	server   *http.Server
	stopped  chan bool
	done     chan os.Signal

	quit chan os.Signal
}

func NewEchoServer(address string, endpoints ...*Endpoint) *EchoServer {
	return &EchoServer{Address: address, Endpoints: endpoints}
}

func (s *EchoServer) SetEndpoints(endpoints []*Endpoint) {
	s.Endpoints = endpoints
}

func (s *EchoServer) SetEndpoint(endpoint *Endpoint) {
	s.Endpoints = append(s.Endpoints, endpoint)
}

func (s *EchoServer) Start() error {
	return s.StartWithWaitingGroup(nil)
}

func (s *EchoServer) StartWithWaitingGroup(wg *sync.WaitGroup) error {
	// We initialize our stopping channels
	s.done = make(chan os.Signal, 1)
	s.stopped = make(chan bool)

	// We create a dedicated server
	mux := http.NewServeMux()

	// We create the server
	s.server = &http.Server{
		Addr:    s.Address,
		Handler: mux,
	}

	// We put our handler
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		s.Handle(w, r)
	})

	// We listen for SIGTERM
	signal.Notify(s.done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// We start the server
	go s.start(wg)

	return nil
}

func (s *EchoServer) onInterrupted() {
	<-s.done
	_ = s.Stop()
}

func (s *EchoServer) start(wg *sync.WaitGroup) {
	// We handle explicit interruption
	go s.onInterrupted()

	// We start the HTTP server
	err := s.server.ListenAndServe()

	// We handle the graceful shutdown (so all requests in the wg are processed)
	if wg != nil {
		if err == http.ErrServerClosed {
			wg.Wait()
		}
	}

	// We display the proper error
	if err != nil && err != http.ErrServerClosed {
		log.Fatal(fmt.Errorf("could not start the echo server (keep in mind that it's running in a go routing during tests): %w", err))
	}

	// We inform that the server has stopped
	s.stopped <- true
	close(s.done)
}

func (s *EchoServer) Stop() error {
	// We stop here when there is no server
	if s.server == nil {
		return nil
	}

	// We shut down the server
	if err := s.server.Shutdown(context.Background()); err != nil {
		return err
	}

	// We wait for the stopped channel to indicate that the server has stopped
	<-s.stopped

	return nil
}

func (s *EchoServer) URL(uri string) string {
	return fmt.Sprintf("http://%s/%s", s.Address, strings.TrimPrefix(uri, "/"))
}
