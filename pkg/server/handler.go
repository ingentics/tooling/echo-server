package server

import (
	"fmt"
	"net/http"
	"strings"
)

func (s *EchoServer) Handle(writer http.ResponseWriter, request *http.Request) {
	// We search for the related endpoint
	endpoint := s.findEndpoint(request)
	// When nothing was found, we simply return a 404
	if nil == endpoint {
		http.Error(writer, fmt.Sprintf("404 page not found `%s %s`", request.Method, request.RequestURI), http.StatusNotFound)
		return
	}

	// We add the content type if there is such
	if len(endpoint.ContentType) > 0 {
		writer.Header().Set("Content-Type", endpoint.ContentType)
	}

	// Write the response status code
	if endpoint.StatusCode == 0 {
		endpoint.StatusCode = http.StatusOK
	}
	writer.WriteHeader(int(endpoint.StatusCode))

	// We stop here if there is no response content
	if len(endpoint.ResponseBody) == 0 {
		return
	}

	// Write the response body
	if _, err := writer.Write([]byte(endpoint.ResponseBody)); err != nil {
		http.Error(writer, "Failed to write response", http.StatusInternalServerError)
	}
}

func (s *EchoServer) findEndpoint(request *http.Request) *Endpoint {
	// We search for the related endpoint
	for _, endpoint := range s.Endpoints {
		if request.Method == strings.ToUpper(endpoint.Method) && request.RequestURI == endpoint.Uri {
			return endpoint
		}
	}
	return nil
}
