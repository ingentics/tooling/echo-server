package server

import (
	"net/http"
	"strings"
)

type Endpoint struct {
	Method       string `json:"method"`
	Uri          string `json:"uri"`
	ResponseBody string `json:"response_body"`
	StatusCode   int    `json:"status_code"`
	ContentType  string `json:"content_type"`
}

func NewEndpoint(method, uri, response string) *Endpoint {
	statusCode := http.StatusOK
	var contentType string
	if len(response) == 0 {
		statusCode = http.StatusNoContent
	} else {
		if strings.HasPrefix(response, "{") {
			contentType = "application/json"
		}
	}

	return &Endpoint{
		Method:       method,
		Uri:          uri,
		ResponseBody: response,
		StatusCode:   statusCode,
		ContentType:  contentType,
	}
}

func (e *Endpoint) SetContentType(contentType string) *Endpoint {
	e.ContentType = contentType
	return e
}

func (e *Endpoint) SetStatusCode(statusCode int) *Endpoint {
	e.StatusCode = statusCode
	return e
}
