package server

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	"testing"
)

func TestEchoServer(t *testing.T) {
	port := 5051

	// Helpers
	url := func(uri string) string {
		return fmt.Sprintf("http://127.0.0.1:%d/%s", port, strings.TrimPrefix(uri, "/"))
	}
	get := func(uri string) (string, error) {
		response, err := http.Get(url(uri))
		if err != nil {
			return "", err
		}
		raw, err := io.ReadAll(response.Body)
		return string(raw), nil
	}
	post := func(uri, body string) (string, error) {
		response, err := http.Post(url(uri), "application/json", strings.NewReader(body))
		if err != nil {
			return "", err
		}
		raw, err := io.ReadAll(response.Body)
		return string(raw), nil
	}

	// We create the server
	server := NewEchoServer(fmt.Sprintf("127.0.0.1:%d", port),
		NewEndpoint("GET", "/test/1", `{"test": "1"}`),
		NewEndpoint("POST", "/test/2", `{"test": "2"}`),
	)
	defer server.Stop()

	// We start it (it will wait for wg on server Stop)
	if err := server.Start(); err != nil {
		t.Fatal(err)
	}

	out, err := get("/test/1")
	if err != nil {
		t.Errorf("/test/1 call failed: %v", err)
	}
	if out != `{"test": "1"}` {
		t.Errorf("/test/1 invalid result: %v", out)
	}

	out, err = post("/test/2", "")
	if err != nil {
		t.Errorf("/test/2 call failed: %v", err)
	}
	if out != `{"test": "2"}` {
		t.Errorf("/test/2 invalid result: %v", out)
	}
}

/*
func TestEchoServer_WaitingGroup(t *testing.T) {
	port := 5052

	// Helpers
	url := func(uri string) string {
		return fmt.Sprintf("http://127.0.0.1:%d/%s", port, strings.TrimPrefix(uri, "/"))
	}
	get := func(uri string, wg *sync.WaitGroup) (string, error) {
		defer wg.Done()
		response, err := http.Get(url(uri))
		if err != nil {
			return "", err
		}
		raw, err := io.ReadAll(response.Body)
		return string(raw), nil
	}
	post := func(uri, body string, wg *sync.WaitGroup) (string, error) {
		defer wg.Done()
		response, err := http.Post(url(uri), "application/json", strings.NewReader(body))
		if err != nil {
			return "", err
		}
		raw, err := io.ReadAll(response.Body)
		return string(raw), nil
	}

	// We create the server
	server := NewEchoServer(fmt.Sprintf("127.0.0.1:%d", port),
		NewEndpoint("GET", "/test/1", `{"test": "1"}`),
		NewEndpoint("POST", "/test/2", `{"test": "2"}`),
	)
	defer server.Stop()

	// We start it (it will wait for wg on server Stop)
	var wg sync.WaitGroup
	if err := server.StartWithWaitingGroup(&wg); err != nil {
		t.Fatal(err)
	}

	wg.Add(1)
	out, err := get("/test/1", &wg)
	if err != nil {
		t.Errorf("/test/1 call failed: %v", err)
	}
	if out != `{"test": "1"}` {
		t.Errorf("/test/1 invalid result: %v", out)
	}

	wg.Add(1)
	out, err = post("/test/2", "", &wg)
	if err != nil {
		t.Errorf("/test/2 call failed: %v", err)
	}
	if out != `{"test": "2"}` {
		t.Errorf("/test/2 invalid result: %v", out)
	}
}
*/
