module gitlab.com/ingentics/tooling/echo-server.git

go 1.19

require gopkg.in/yaml.v2 v2.4.0
